# Space Breakout #

## Summary ##

This is a project for CSCI4140 in CUHK. The product is a 2-player breakout game with dynamic block adding.

## Features ##

1. Real-time synchronized gameplay
2. Item usage
3. Dynamic block adding during gameplay

## Setup ##

### Client ###
* Public - Navigate to ```[ip address]:44140``` to play with a waiting anonymous.
* Private
    * Create game room by entering ```[ip address]:44140/id```, the game will not put anonymous into your room automatically. Note: ```id``` should be integer greater than ```999```
    * Join a private room by entering ```[ip address]:44140/id```

### Server ###

1. Clone this project
2. Navigate inside the folder with Command Prompt
3. Run ``` npm install ```, then ``` node app.js ``` to start the server
4. Follow the client side instruction to enter the game

### Port ###

By default, this project use port ```44140```. You can change it in ```app.js```.

## Components ##
* Node.js [https://nodejs.org/](https://nodejs.org/)
* box2d.js [https://github.com/kripken/box2d.js/](https://github.com/kripken/box2d.js/)

## Milestone / Marking (Total 30) ##
*Extra 5 marks are for professor's comment, making total as 35.*

1. Game building
    1. Basic game structure (10)
    2. Two player game (5)
2. Basic collision effect (5)
3. Items implementation - length, block HP, change color (5)
4. Dynamic block adding (5)