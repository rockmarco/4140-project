// Declare variables
// These are for use across functions
var world;

var balls = new Array(0);
var ball_width = 30.0;
var BoardWidth = 150.0;
var BoardHeight = 10.0;
var velocity = 10;
var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
var CANVAS_WIDTH = canvas.width, CANVAS_HEIGHT = canvas.height, SCALE = 30;
var rect = canvas.getBoundingClientRect();
var CANVAS_X = rect.left, CANVAS_Y = rect.top;

// Declare texture
// Red ball image
var redBallImage = new Image();
redBallImage.src = "../images/red-ball.png";
// Blue ball image
// Not implemented

// Declare breakout board
var redBoard;

// Capture mouse position
var mouse = {x:0, y:0};

canvas.addEventListener('mousemove', function(e){
    mouse.x = e.clientX || e.pageX;
    mouse.y = e.clientY || e.pageY;
    // Prevent board out of canva
    // Note the position is count at center
    // So divide the width by 2
    if (mouse.x < BoardWidth/2) mouse.x = BoardWidth/2;
    if (mouse.x > CANVAS_WIDTH - BoardWidth/2) mouse.x = CANVAS_WIDTH - BoardWidth/2;
}, false);

// Setup alias for Box2D functions
var     b2Vec2 = Box2D.Common.Math.b2Vec2
    ,   b2AABB = Box2D.Collision.b2AABB
    ,	b2BodyDef = Box2D.Dynamics.b2BodyDef
    ,	b2Body = Box2D.Dynamics.b2Body
    ,	b2FixtureDef = Box2D.Dynamics.b2FixtureDef
    ,	b2Fixture = Box2D.Dynamics.b2Fixture
    ,	b2World = Box2D.Dynamics.b2World
    ,	b2MassData = Box2D.Collision.Shapes.b2MassData
    ,	b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape
    ,	b2CircleShape = Box2D.Collision.Shapes.b2CircleShape
    ,	b2DebugDraw = Box2D.Dynamics.b2DebugDraw
    ,   b2MouseJointDef =  Box2D.Dynamics.Joints.b2MouseJointDef
    ;

// Init method call when page load
// Init Everything inside
function init() {
    // Prevent moving object stops
    //Box2D.Common.b2Settings.b2_velocityThreshold = 0;

    // Create Box2D World
    world = new b2World(
        new b2Vec2(0, 0)    //gravity
        ,  true                 //allow sleep
    );

    // Define properties for objects
    var fixDef = new b2FixtureDef;
    fixDef.density = 1.0;
    // No friction to prevent object slow down / rotate when collision
    fixDef.friction = 0;    //0.5;
    // Restitution to 1 for complete elastic object
    fixDef.restitution = 1; //0.2;

    var bodyDef = new b2BodyDef();

    // Create borders
    // Bottom
    bodyDef.type = b2Body.b2_staticBody;
    bodyDef.position.x = CANVAS_WIDTH / 2.0 / SCALE;
    bodyDef.position.y = CANVAS_HEIGHT / SCALE;
    fixDef.shape = new b2PolygonShape;
    fixDef.shape.SetAsBox(CANVAS_WIDTH / SCALE, 0);
    world.CreateBody(bodyDef).CreateFixture(fixDef);
    // Top
    bodyDef.type = b2Body.b2_staticBody;
    bodyDef.position.x = CANVAS_WIDTH / 2.0 / SCALE;
    bodyDef.position.y = 0;
    fixDef.shape = new b2PolygonShape;
    fixDef.shape.SetAsBox(CANVAS_WIDTH / SCALE, 0);
    world.CreateBody(bodyDef).CreateFixture(fixDef);
    // Left
    bodyDef.type = b2Body.b2_staticBody;
    bodyDef.position.x = 0;
    bodyDef.position.y = CANVAS_HEIGHT / SCALE / 2.0;
    fixDef.shape = new b2PolygonShape;
    fixDef.shape.SetAsBox(0, CANVAS_HEIGHT/SCALE);
    world.CreateBody(bodyDef).CreateFixture(fixDef);
    // Right
    bodyDef.type = b2Body.b2_staticBody;
    bodyDef.position.x = CANVAS_WIDTH / SCALE;
    bodyDef.position.y = CANVAS_HEIGHT / SCALE / 2.0;
    fixDef.shape = new b2PolygonShape;
    fixDef.shape.SetAsBox(0, CANVAS_HEIGHT/SCALE);
    world.CreateBody(bodyDef).CreateFixture(fixDef);

    // TEST: Create breakout board
    bodyDef.type = b2Body.b2_staticBody;
    bodyDef.position.x = CANVAS_WIDTH / SCALE / 2.0;
    bodyDef.position.y = (CANVAS_HEIGHT-(BoardHeight/2)) / SCALE;
    fixDef.shape = new b2PolygonShape;
    fixDef.shape.SetAsBox(BoardWidth/SCALE/2, BoardHeight/SCALE/2);
    redBoard = world.CreateBody(bodyDef).CreateFixture(fixDef);

    //create some objects
    bodyDef.type = b2Body.b2_dynamicBody;
    for(var i = 0; i < 10; ++i) {
        fixDef.shape = new b2CircleShape(
            //Math.random() + 0.1 //radius
            ball_width / 2 / SCALE
        );

        // Setup velocity
        bodyDef.linearVelocity.x = 10;
        bodyDef.linearVelocity.y = 10;
        // Position the ball
        bodyDef.position.x = Math.random() * 10;
        bodyDef.position.y = Math.random() * 10;
        // Save the balls for future use (e.g. update() )
        balls.push(world.CreateBody(bodyDef).CreateFixture(fixDef));
    }

    //setup debug draw
    // DELETE when release
    var debugDraw = new b2DebugDraw();
     debugDraw.SetSprite(document.getElementById("canvas").getContext("2d"));
     debugDraw.SetDrawScale(SCALE);
     debugDraw.SetFillAlpha(0.3);
     debugDraw.SetLineThickness(1.0);
     debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
     world.SetDebugDraw(debugDraw);

    // Set timer to call update
    window.setInterval(update, 1000 / 60);

    // Debugging
    console.log(balls[0]);

};

function update() {
    world.Step(
        1 / 60   //frame-rate
        ,  10       //velocity iterations
        ,  10       //position iterations
    );
    for (var i in balls) {
        var b = balls[i];
        //if (Math.abs(b.m_body.m_linearVelocity.x) != velocity) b.m_body.m_linearVelocity.x = (b.m_body.m_linearVelocity.x / Math.abs(b.m_body.m_linearVelocity.x)) * velocity;
        //if (Math.abs(b.m_body.m_linearVelocity.y) != velocity) b.m_body.m_linearVelocity.y = (b.m_body.m_linearVelocity.y / Math.abs(b.m_body.m_linearVelocity.y)) * velocity;
        if (b.m_body.m_linearVelocity.x >= 0 && b.m_body.m_linearVelocity.x != velocity) {
                b.m_body.m_linearVelocity.x = velocity;
        } else if (b.m_body.m_linearVelocity.x < 0 && b.m_body.m_linearVelocity.x != -(velocity)) {
            b.m_body.m_linearVelocity.x = -(velocity);
        }
        if (b.m_body.m_linearVelocity.y >= 0 && b.m_body.m_linearVelocity.y != velocity) {
            b.m_body.m_linearVelocity.y = velocity;
        } else if (b.m_body.m_linearVelocity.y < 0 && b.m_body.m_linearVelocity.y != -(velocity)) {
            b.m_body.m_linearVelocity.y = -(velocity);
        }
    }

    var redBoardBody = redBoard.GetBody();
    redBoardBody.SetPositionAndAngle(new b2Vec2(mouse.x / SCALE, redBoardBody.GetPosition().y), redBoardBody.GetAngle());
    //console.log(redBoardBody.GetPosition().x + '; ' + redBoardBody.GetPosition().y);

    //velocity += 0.01;
    world.DrawDebugData();
    world.ClearForces();

    // Clear Debug images (shapes)
    //context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

    // Apply textures
    for (var i in balls) {
        var pos = balls[i].GetBody().GetPosition();
        context.save();
        context.translate(pos.x * SCALE, pos.y * SCALE);
        context.rotate(balls[i].GetBody().GetAngle());
        context.drawImage(redBallImage, -(ball_width/2), -(ball_width/2));
        context.restore();
    }
};

function addBox() {
    for (var i = 0; i < 20; i++) {
        // Define properties for objects
        var fixDef = new b2FixtureDef;
        fixDef.density = 1.0;
        // No friction to prevent object slow down / rotate when collision
        fixDef.friction = 0;    //0.5;
        // Restitution to 1 for complete elastic object
        fixDef.restitution = 1; //0.2;

        var bodyDef = new b2BodyDef;

        // Create borders
        // Bottom
        bodyDef.type = b2Body.b2_staticBody;
        bodyDef.position.x = (Math.random() * CANVAS_WIDTH) / SCALE;
        bodyDef.position.y = (Math.random() * CANVAS_HEIGHT) / SCALE;
        fixDef.shape = new b2PolygonShape;
        fixDef.shape.SetAsBox(30 / SCALE, 30 / SCALE);
        world.CreateBody(bodyDef).CreateFixture(fixDef);
    }
}