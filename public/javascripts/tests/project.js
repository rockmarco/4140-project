var project = function() {
    //constructor
}

project.prototype.setNiceViewCenter = function() {
    //called once when the user changes to this test from another test
    PTM = 32;
    setViewCenterWorld( new b2Vec2(0,0), true );
}

// Create Global variables
var life = 3;
var init_blocks_col = 6;
var init_blocks_row = 4;
var shouldBallsCollide = true;
var myBall = {
    body: {},
    fixture: {},
    image: {},
    radius: 10,
    velocity: 5, // Max x / y velocity -> (If =5, then e.g. [5,2] / [3.9,5] / [4,4]) but not [6,2].)
    scheduleDestroy: false,
	damage: 1
}
var enemyBall = {
    body: {},
    fixture: {},
    image: {},
    radius: 10,
    velocity: 5,
    scheduleDestroy: false,
	damage: 1
}
var myBoard = {
    body: {},
    fixture: {},
    image: {},
    width: 150, //keep track of original size so we can change back after use item
    height: 10
}
var enemyBoard = {
    body: {},
    fixture: {},
    image: {},
    width: 150,
    height: 10
}
var block_temp = {
	body: {},
	fixture: {},
	image: {},
	type: {},
	hp: 0,
	extra: null
}
var small_block = {
	img_src: "/images/block-small.png",
	width: 50,
	height: 25,
	hp: 1,
	ep_cost: 1
}
var medium_block = {
	img_src: "/images/block-medium.png",
	width: 100,
	height: 25,
	hp: 2,
	ep_cost: 2
}
var large_block = {
	img_src: "/images/block-large.png",
	width: 200,
	height: 25,
	hp: 5,
	ep_cost: 4
}
var dynamicBlock = null;
var enemyBlocks = new Array();  // Array of block with hp
var myBlocks = new Array();
var destroyList = new Array();
/*
Here is a template of block object
{
    body: {},
    fixture: {},
    image: {},
        // No width / height here, since it will not be change once created
    hp: 10  // This is HP of the block, should be different among different blocks (e.g. 5 for small castle, 10 for big castle)
}

!!!Remember to add to the array after create!!!
 */
var myDeadline = {
    body: {},
    fixture: {},
    image: {}
}
var enemyDeadline = {
    body: {},
    fixture: {},
    image: {}
}

project.prototype.setup = function() {
    //set up images
    myBall.image = new Image();
    myBall.image.src = "/images/blue-ball.png";
    myBoard.image = new Image();
    myBoard.image.src = "/images/blue-square.png";
    enemyBall.image = new Image();
    enemyBall.image.src = "/images/red-ball.png";
    enemyBoard.image = new Image();
    enemyBoard.image.src = "/images/red-square.png";

    //set up the Box2D scene here - the world is already created
    var bodyDef = new b2BodyDef();
    var body, shape;

    // Create world borders
    // Left
    bodyDef.set_type(Module.b2_staticBody);
    bodyDef.set_position(new b2Vec2(0, canvas.height / 2.0 / PTM));
    body = world.CreateBody(bodyDef);
    shape = new Box2D.b2PolygonShape();
    shape.SetAsBox(0 / PTM,canvas.height / PTM);
    body.CreateFixture(shape, 1);
    // Bottom
    bodyDef.set_type(Module.b2_staticBody);
    bodyDef.set_position(new b2Vec2(canvas.width / 2.0 / PTM, 0));
    myDeadline.body = world.CreateBody(bodyDef);
    shape = new Box2D.b2PolygonShape();
    shape.SetAsBox(canvas.width / PTM,0/PTM);
    myDeadline.fixture = myDeadline.body.CreateFixture(shape, 1);
    // Right
    bodyDef.set_type(Module.b2_staticBody);
    bodyDef.set_position(new b2Vec2(canvas.width / PTM, canvas.height / 2.0 / PTM));
    body = world.CreateBody(bodyDef);
    shape = new Box2D.b2PolygonShape();
    shape.SetAsBox(0/PTM,canvas.height / PTM);
    body.CreateFixture(shape, 1);
    // Top
    bodyDef.set_type(Module.b2_staticBody);
    bodyDef.set_position(new b2Vec2(canvas.width / 2.0 / PTM, canvas.height / PTM));
    enemyDeadline.body = world.CreateBody(bodyDef);
    shape = new Box2D.b2PolygonShape();
    shape.SetAsBox(canvas.width / PTM,0/PTM);
    enemyDeadline.fixture = enemyDeadline.body.CreateFixture(shape, 1);

    // Create boards
    // Bottom
    bodyDef.set_type(Module.b2_staticBody);
    bodyDef.set_position(new b2Vec2(canvas.width / 2.0 / PTM, (myBoard.height / 2.0 + 50) / PTM));
	mousePosWorld.x = canvas.width / 2.0 / PTM;
    myBoard.body = world.CreateBody(bodyDef);
    shape = new Box2D.b2PolygonShape();
    shape.SetAsBox(myBoard.width/2.0 / PTM, myBoard.height/2.0/PTM);
    myBoard.fixture = myBoard.body.CreateFixture(shape, 1);
    // Top
    bodyDef.set_type(Module.b2_staticBody);
    bodyDef.set_position(new b2Vec2(canvas.width / 2.0 / PTM, (canvas.height - 50 - enemyBoard.height / 2.0) / PTM));
    enemyPos.x = canvas.width / 2.0;
    enemyBoard.body = world.CreateBody(bodyDef);
    shape = new Box2D.b2PolygonShape();
    shape.SetAsBox(enemyBoard.width/2.0 / PTM, enemyBoard.height/2.0/PTM);
    enemyBoard.fixture = enemyBoard.body.CreateFixture(shape, 1);

	// Create Blocks
	initBlocks(canvas.width / 2.0, canvas.height / 4.0, small_block, init_blocks_col, init_blocks_row, 5, 5, myBlocks, false);
	initBlocks(canvas.width / 2.0, canvas.height * 3 / 4.0, small_block, init_blocks_col, init_blocks_row, 5, 5, enemyBlocks, true);

    // Create Balls
    var tempBall = createBall(myBoard.body.GetPosition().get_x() - (30 / PTM), myBoard.body.GetPosition().get_y() + (30/PTM), myBall.radius/PTM, myBall.velocity, myBall.velocity);
    myBall.body = tempBall.body;
    myBall.fixture = tempBall.fixture;

    tempBall = createBall(enemyBoard.body.GetPosition().get_x() + (30 / PTM), enemyBoard.body.GetPosition().get_y() - (30/PTM), enemyBall.radius/PTM, -enemyBall.velocity, -enemyBall.velocity);
    enemyBall.body = tempBall.body;
    enemyBall.fixture = tempBall.fixture;

    // Filter part
    var filter = new Box2D.JSContactFilter();
    filter.ShouldCollide = function (fixA, fixB) {
		/*
		Note the params given to this function is a number indicate the object
		To compare it, use the 'e' key in stored object
		e.g.
		If I want the enemyBall to pass through my board, do as follows
		if ( (fixA == myBoardFix.e || fixB == myBoardFix.e) &&
			(fixA == enemyBallFix.e || fixB == enemyBallFix.e) ) {
			return false;
		}
		*** Remember to return true at the end... ***
		 */
		
		if (dynamicBlock && (fixA == dynamicBlock.fixture.e || fixB == dynamicBlock.fixture.e)) {
			return false;
		}
		
		for (var key in myBlocks) {
			var block = myBlocks[key];
			if (isCollide(fixA, fixB, block, enemyBall)) {
				return true;
			}
			if (isCollide(fixA, fixB, block, myBall)) {
				return false;
            }
		}
			
		for (var key in enemyBlocks) {
			var block = enemyBlocks[key];
			if (isCollide(fixA, fixB, block, myBall)) {
				return true;
			}
			if (isCollide(fixA, fixB, block, enemyBall)) {
				return false;
			}
		}

        if ((fixA == myBoard.fixture.e || fixB == myBoard.fixture.e) &&
            (fixA == enemyBall.fixture.e || fixB == enemyBall.fixture.e)) {
            return false;
        }
        if ((fixA == enemyBoard.fixture.e || fixB == enemyBoard.fixture.e) &&
            (fixA == myBall.fixture.e || fixB == myBall.fixture.e)) {
            return false;
        }
        // myBall pass through myDeadline
        if ((fixA == myDeadline.fixture.e || fixB == myDeadline.fixture.e) &&
            (fixA == myBall.fixture.e || fixB == myBall.fixture.e)) {
            myBall.scheduleDestroy = true;
            return false;
        }
        // enemyBall pass through enemyDeadline
        if ((fixA == enemyDeadline.fixture.e || fixB == enemyDeadline.fixture.e) &&
            (fixA == enemyBall.fixture.e || fixB == enemyBall.fixture.e)) {
            enemyBall.scheduleDestroy = true;
            return false;
        }
        if ((fixA == myBall.fixture.e || fixB == myBall.fixture.e) &&
            (fixA == enemyBall.fixture.e || fixB == enemyBall.fixture.e)) {
            return false;
        }

        return true;    // Final line make all other things collide
    }
    world.SetContactFilter(filter);
	
	var listener = new Box2D.JSContactListener;
	
	listener.BeginContact = function (contact) {
		
	}
	
	listener.EndContact = function (contact) {
		contact = Module.wrapPointer(contact, Module.b2Contact);

		var fixA = contact.GetFixtureA().e;
		var fixB = contact.GetFixtureB().e;

		if (fixA == myBoard.fixture.e || fixB == myBoard.fixture.e ||
			fixA == enemyBoard.fixture.e || fixB == enemyBoard.fixture.e) {
			boardAudio.currentTime = 0;
			boardAudio.play();
		} else {
			collideAudio.currentTime = 0;
			collideAudio.play();
		}

		if (fixA == myBall.fixture.e || fixB == myBall.fixture.e) {
			if (myBall.body.GetLinearVelocity().get_y() > 0) {	//go upward, leave deadline
				myBall.scheduleDestroy = false;
			}
		}
		if (fixA == enemyBall.fixture.e || fixB == enemyBall.fixture.e) {
			if (enemyBall.body.GetLinearVelocity().get_y() < 0) {	//go downward, leave deadline
				enemyBall.scheduleDestroy = false;
			}
		}
		syncBall(myBall, enemyBlocks, fixA, fixB, "sync-ball");
		for (var key in enemyBlocks) {
			var block = enemyBlocks[key];
			if (isCollide(fixA, fixB, block, myBall)) {
				damage(block, enemyBlocks, myBall.damage);
			}
		}
        for (var key in myBlocks) {
            var block = myBlocks[key];
            if (isCollide(fixA, fixB, block, enemyBall)) {
                shouldBallsCollide = false;
            }
        }
	}
	
	listener.PreSolve = function (contact, impulse) {
		contact = Module.wrapPointer(contact, Module.b2Contact);

		var fixA = contact.GetFixtureA().e;
		var fixB = contact.GetFixtureB().e;
		
		if (laser.isActive) {
			for (var key in enemyBlocks) {
				var block = enemyBlocks[key];
				if (isCollide(fixA, fixB, block, myBall)) {
					if (block.hp <= myBall.damage) {
						contact.SetEnabled(false);
					}
				}
			}
		}
		if (laser.isEnemyActive) {	
			for (var key in myBlocks) {
				var block = myBlocks[key];
				if (isCollide(fixA, fixB, block, enemyBall)) {
					if (block.hp <= enemyBall.damage) {
						contact.SetEnabled(false);
					}
				}
			}
		}
	}
	
	listener.PostSolve = function (contact, oldManifold) {
		
	}
	
	world.SetContactListener(listener);
}

function initBlocks(center_x, center_y, block_type, col, row, offset_x, offset_y, blocks, shouldRotate) {
	var init_x;
	var init_y;
	if (!shouldRotate) {
		init_x = center_x - (block_type.width + offset_x) * (col - 1) / 2.0;
		init_y = center_y - (block_type.height + offset_y) * (row - 1) / 2.0;
	} else {
		init_x = center_x + (block_type.width + offset_x) * (col - 1) / 2.0;
		init_y = center_y + (block_type.height + offset_y) * (row - 1) / 2.0;
	}
	var y = init_y;
	for (var i = 0; i < row; i++) {
		var x = init_x;
		for (var j = 0; j < col; j++) {
			var angle = (shouldRotate) ? Math.PI : 0;
			blocks.push(createBlock(block_type, null, x / PTM, y / PTM, angle));
			if (!shouldRotate) {
				x += block_type.width + offset_x;
			} else {
				x -= block_type.width + offset_x;
			}
		}
		if (!shouldRotate) {
			y += block_type.height + offset_y;
		} else {
			y -= block_type.height + offset_y;
		}
	}
}

function isCollide(fixA, fixB, objA, objB) {
	return ((fixA == objA.fixture.e || fixB == objA.fixture.e) &&
			(fixA == objB.fixture.e || fixB == objB.fixture.e));
}

function damage(block, blocks, damage) {
	if (block.hp > 0) {
		block.hp -= damage;
		if (block.hp <= 0) {
			blocks.splice(blocks.indexOf(block), 1);
			destroyList.push(block);
		}
	}
}

function destroy() {
	while (destroyList.length) {
		destroyBlock(destroyList.pop());
	}
}


function syncBall(ball, blocks, fixA, fixB, socket_key) {
	if (fixA == ball.fixture.e || fixB == ball.fixture.e) {
		var velocity = ball.body.GetLinearVelocity();
		var max = Math.max(Math.abs(velocity.get_x()), Math.abs(velocity.get_y()));
		if (max != ball.velocity) {
			var ratio = ball.velocity / max;
			ball.body.SetLinearVelocity(new b2Vec2(velocity.get_x() * ratio, velocity.get_y() * ratio));
		}
		
		var data = {};
		data.pos_x = ball.body.GetPosition().get_x();
		data.pos_y = ball.body.GetPosition().get_y();
		data.angle = ball.body.GetAngle();
		data.linearVelocity_x = ball.body.GetLinearVelocity().get_x();
		data.linearVelocity_y = ball.body.GetLinearVelocity().get_y();
		
		for (var key in blocks) {
			var block = blocks[key];
			if (fixA == block.fixture.e || fixB == block.fixture.e) {
				data.block_index = key;
			}
		}
		
		socket.emit(socket_key, data);
	}
}

var isMax = false;  //Max in HP or not

function changeItemsIcon(itemName, requiredEp) {
    if (ep < requiredEp) {
        exchangeIcon(itemName, false);
    }
    else {
        switch (itemName) {
            case "increaseDamage":
                exchangeIcon(itemName, true);
                break;
            case "recover":
                if (isMax)
                    exchangeIcon(itemName, false);
                else
                    exchangeIcon(itemName, true);
                break;
            case "broaden":
                if (ibl.isActive)
                    exchangeIcon(itemName, false);
                else
                    exchangeIcon(itemName, true);
                break;
            case "changeColor":
                if (cbc.isActive)
                    exchangeIcon(itemName, false);
                else
                    exchangeIcon(itemName, true);
                break;
            case "laser":
                if (laser.isActive)
                    exchangeIcon(itemName, false);
                else
                    exchangeIcon(itemName, true);
                break;
        }
    }
}

function exchangeIcon(itemName, isEnable) {
    var item = document.getElementById(itemName);
    if (item) {
        var index = item.src.lastIndexOf("/");
        var pathOfItem = item.src.substring(0, index+1) + itemName + ".jpg";
        var pathOfItemDisabled = item.src.substring(0, index+1) + itemName + "_disabled.jpg";
        if (isEnable) {
            if (item.src === pathOfItemDisabled)
                item.src = pathOfItem;
        }
        else {
            if (item.src === pathOfItem)
                item.src = pathOfItemDisabled;
        }
    }
}

project.prototype.step = function() {
    //this function will be called at the beginning of every time step
    var pos = myBoard.body.GetPosition();
    myBoard.body.SetTransform(new b2Vec2(mousePosWorld.x, pos.get_y()), myBoard.body.GetAngle());

    pos = enemyBoard.body.GetPosition();
    var x = enemyPos.x;  // Opposite x
    enemyBoard.body.SetTransform(new b2Vec2(x / PTM, pos.get_y()), enemyBoard.body.GetAngle());
	if (dynamicBlock) {
		var y = Math.min((canvas.height - dynamicBlock.type.height) / 2.0 / PTM, mousePosWorld.y);
		dynamicBlock.body.SetTransform(new b2Vec2(mousePosWorld.x, y), dynamicBlock.body.GetAngle());
	}
	for (var key in myBlocks) {
		var extra = myBlocks[key].extra;
		if (extra && typeof(extra) === "function") {
			extra();
		}
	}
	for (var key in enemyBlocks) {
		var extra = enemyBlocks[key].extra;
		if (extra && typeof(extra) === "function") {
			extra();
		}
	}
	document.title = "LIFE: " + life +" EP: " + ep;

    if (myBall.scheduleDestroy && myBall.body.GetPosition().get_y() <= myBall.radius/PTM ) {
        myBall.body.GetWorld().DestroyBody(myBall.body);
        myBall.body = {};
        myBall.fixture = {};
        myBall.scheduleDestroy = false;
        life--;
    }
    if (enemyBall.scheduleDestroy && enemyBall.body.GetPosition().get_y() >= (canvas.height - enemyBall.radius)/PTM) {
        enemyBall.body.GetWorld().DestroyBody(enemyBall.body);
        enemyBall.body = {};
        enemyBall.fixture = {};
        enemyBall.scheduleDestroy = false;
    }

    if (life <= 0) {
        socket.emit('life-zero', null);
        endGame('Your life reach 0.<br/>You lose.', 'You lose.');
    }
	
	if (myBlocks.length <= 0) {
        endGame('You have no block.<br/>You lose.', 'You lose.');
	}
	
	if (enemyBlocks.length <= 0) {
        endGame('You destroy all enemy\'s blocks.<br/>You win.', 'You win.');
	}

	var myEp = document.getElementById("myEP");
	myEp.innerHTML = "" + ep;
	var myLife = document.getElementById("myLife");
	myLife.innerHTML = "" + life;
	var myDamage = document.getElementById("myDamage");
	myDamage.innerHTML = "X" + myBall.damage;
	changeItemsIcon("broaden", ibl.ep);
	changeItemsIcon("recover", rbh.ep);
	changeItemsIcon("changeColor", cbc.ep);
    changeItemsIcon("laser", laser.ep);
    changeItemsIcon("increaseDamage", ibd.ep);
}

function destroy() {
	while (destroyList.length) {
		destroyBlock(destroyList.pop());
	}
}

project.prototype.draw = function() {
    //this function will be called at the end of every time step
	destroy();
	drawBorder(canvas);
    for (var key in myBlocks) {
        renderBox(myBlocks[key], myBlocks[key].type.width, myBlocks[key].type.height);
    }

    for (var key in enemyBlocks) {
        renderBox(enemyBlocks[key], enemyBlocks[key].type.width, enemyBlocks[key].type.height);
    }

    renderBox(myBoard, myBoard.width, myBoard.height);
    renderBox(enemyBoard, enemyBoard.width, enemyBoard.height);
    if (myBall.fixture.GetBody) renderBall(myBall.fixture, myBall.image);
    if (enemyBall.fixture.GetBody) renderBall(enemyBall.fixture, enemyBall.image);
	if (dynamicBlock) {
		renderBox(dynamicBlock, dynamicBlock.type.width, dynamicBlock.type.height);
	}
}

project.prototype.onKeyDown = function(canvas, evt) {
	var block_rotate = null;
	var block_type = null;
	
	switch (evt.keyCode) {
        case 32: // 'space'
			evt.preventDefault();
            if (!myBall.fixture.GetBody && life > 0) {
                var tempBall = createBall(myBoard.body.GetPosition().get_x() - (30 / PTM), myBoard.body.GetPosition().get_y() + (30/PTM), myBall.radius/PTM, myBall.velocity, myBall.velocity);
                myBall.body = tempBall.body;
                myBall.fixture = tempBall.fixture;
                socket.emit('create-ball', null);
            } else if (myBall.fixture.GetBody && life > 1) {
				life--;
				myBall.body.GetWorld().DestroyBody(myBall.body);
				var tempBall = createBall(myBoard.body.GetPosition().get_x() - (30 / PTM), myBoard.body.GetPosition().get_y() + (30/PTM), myBall.radius/PTM, myBall.velocity, myBall.velocity);
				myBall.body = tempBall.body;
				myBall.fixture = tempBall.fixture;
				socket.emit('create-ball', null);
			}
            break;
		case 49: // start from '1'
			block_type = block_type || small_block;
		case 50:
			block_type = block_type || medium_block;
		case 51: // to '3'
			block_type = block_type || large_block;
			
			destroyBlock(dynamicBlock);
			dynamicBlock = createBlock(block_type, null);
			break;
		case 65: // 'a'
			block_rotate = block_rotate || 1;
		case 68: // 'd'
			block_rotate = block_rotate || 2;
			
			rotateBox(dynamicBlock, block_rotate, Math.PI / 8.0);
			break;
        case 81: // 'q'
			if (ibl.ep <= ep && !ibl.isActive) {
				ep -= ibl.ep;
				ibl.isActive = true;
				increaseBoardLength(myBoard);
				socket.emit('item-ibl', null);
				window.setTimeout(function() {
					ibl.isActive = false;
				}, ibl.time);
			}
            break;
		case 87: // 'w'
			if (rbh.ep <= ep) {
				var notMax = false;
				for (var key in myBlocks) {
					if (myBlocks[key].hp < myBlocks[key].type.hp + 1) {
						notMax = true;
						break;
					}
				}
                isMax = !notMax;
				if (notMax) {
					ep -= rbh.ep;
					recoverBlockHp(myBlocks);
					socket.emit('item-rbh', null);

					//check for HP again
					notMax = false;
					for (var key in myBlocks) {
						if (myBlocks[key].hp < myBlocks[key].type.hp + 1) {
							notMax = true;
							break;
						}
					}
					isMax = !notMax;
				}
			}
			break;
		case 69: // 'e'
			if (cbc.ep <= ep && !cbc.isActive) {
				ep -= cbc.ep;
				cbc.isActive = true;
				socket.emit('item-cbc', null);
				window.setTimeout(function() {
					cbc.isActive = false;
				}, cbc.time);
			}
			break;
		case 82: // 'r'
			if (laser.ep <= ep && !laser.isActive) {
				ep -= laser.ep;
				socket.emit('item-laser', null);
				changeBallToLaser(myBall, true);
			}
			break;
		case 84: // 't'
			if (ibd.ep <= ep) {
				ep -= ibd.ep;
				socket.emit('item-ibd', null);
				increaseBallDamage(myBall);
			}
			break;
		default:
	}
}

project.prototype.onKeyUp = function(canvas, evt) {
	switch (evt.keyCode) {
		case 65: // 'a'
		default:
	}
}

var rotate_board;

project.prototype.onMouseDown = function(canvas, evt) {
	if (!dynamicBlock) {
		var board_rotate = null;
		switch (evt.which) {
			case 1:
				board_rotate = board_rotate || 1;
			case 3:
				board_rotate = board_rotate || 2;
			
				window.oncontextmenu = function() { return false };
				clearInterval(rotate_board);
				rotate_board = setInterval(function () {
					rotateBox(myBoard, board_rotate, Math.PI / 180.0);
					socket.emit('updateAngle', myBoard.body.GetAngle());
				}, 10);
				break;
			default:
		}
	}
}

project.prototype.onMouseUp = function(canvas, evt) {
	clearInterval(rotate_board);
	if (dynamicBlock) {
		if (evt.which == 1) {
			var newBlock = dynamicBlock;
			if (newBlock.hp_recovery_delay >= 0) {
				newBlock.hp_recovery_time = new Date().getTime() + newBlock.hp_recovery_delay;
			}
			if (newBlock.ep_recovery_delay >= 0) {
				newBlock.ep_recovery_time = new Date().getTime() + newBlock.ep_recovery_delay;
			}
			myBlocks.push(newBlock);
			var encryptedBlock = {
				x: (canvas.width / PTM - newBlock.body.GetPosition().get_x()),
				y: (canvas.height / PTM - newBlock.body.GetPosition().get_y()),
				angle: (newBlock.body.GetAngle() + Math.PI),
				type: newBlock.type,
				hp_recovery_delay: newBlock.hp_recovery_delay,
				hp_recovery_time: newBlock.hp_recovery_time
			}
			socket.emit('newBlock', encryptedBlock);
			dynamicBlock = null;
			ep -= newBlock.type.ep_cost;
		} else if (evt.which == 3) {
			window.oncontextmenu = function() { return false };
			destroyBlock(dynamicBlock);
			dynamicBlock = null;
		}
	}
}

socket.on('newBlock', function (data) {
	var block = createBlock(data.type, null, data.x, data.y, data.angle);
	if (data.hp_recovery_delay) block.hp_recovery_delay = data.hp_recovery_delay;
	if (data.hp_recovery_time) block.hp_recovery_time = data.hp_recovery_time;
	enemyBlocks.push(block);
});

function destroyBlock(block) {
	if (block) {
		block.body.DestroyFixture(block.fixture);
		block.fixture = null;
		block = null;
	}
}

function createBlock(block_type, extra, x, y, angle) {
	if (block_type.ep_cost <= ep) {
		var block = JSON.parse(JSON.stringify(block_temp));
		block.image = new Image();
		block.image.src = block_type.img_src;
		block.type = block_type;
		block.hp = block.type.hp;
		block.extra = extra;
		
		var bodyDef = new b2BodyDef();
		var shape = new Box2D.b2PolygonShape();
		bodyDef.set_type(Module.b2_staticBody);
		if (x && y) {
			bodyDef.set_position(new b2Vec2(x, y));
		} else {
			bodyDef.set_position(new b2Vec2(mousePosWorld.x, mousePosWorld.y));
		}
		if (angle) {
			bodyDef.set_angle(angle);
		}
		block.body = world.CreateBody(bodyDef);
		shape.SetAsBox(block.type.width / 2.0 / PTM, block.type.height / 2.0 / PTM);
		block.fixture = block.body.CreateFixture(shape, 1);
		return block;
	} else {
		return null;
	}
}

function rotateBox(box, direct, offset) {
	if (box) {
		var angle = box.body.GetAngle();
		switch (direct) {
			case 1: angle += offset; break;
			case 2: angle -= offset; break;
		}
		box.body.SetTransform(box.body.GetPosition(), angle);
	}
}

function drawBorder() {
	var thickness = 10;
	context.fillStyle = "#FFFFFF";
	context.fillRect(0, (canvas.height - thickness) / 2.0, canvas.width, thickness);
}

function renderBox(box, width, height) {
    var aabb = box.fixture.GetAABB();
    var x1 = aabb.get_upperBound().get_x();
    var y1 = aabb.get_upperBound().get_y();
    var x2 = aabb.get_lowerBound().get_x();
    var y2 = aabb.get_lowerBound().get_y();
    var center = {};
    center.x = (x1 + x2)/2.0;
    center.y = (y1 + y2)/2.0;

    var body = box.fixture.GetBody();
    var pos = body.GetPosition();
    context.save();
    context.translate(pos.get_x() * PTM, canvas.height - pos.get_y() * PTM);
    context.rotate(-body.GetAngle());
    context.drawImage(box.image, -width / 2.0, -height / 2.0, width, height);
    context.restore();
}

function renderBall(fixture, image) {
    var body = fixture.GetBody();
    var shape = fixture.GetShape();
    var pos = body.GetPosition();
    context.save();
    context.translate(pos.get_x() * PTM, canvas.height - pos.get_y() * PTM);
    context.rotate(body.GetAngle());
    context.drawImage(image, -(shape.get_m_radius()*PTM), -(shape.get_m_radius()*PTM), shape.get_m_radius()*PTM*2, shape.get_m_radius()*PTM*2);
    context.restore();
}

function createBall(x_meter, y_meter, radius_meter, x_velocity, y_velocity) {
    var bodyDef = new b2BodyDef();
    bodyDef.set_type(Module.b2_dynamicBody);
	//check border
	if (x_meter <= radius_meter) x_meter = radius_meter;
	else if (x_meter >= canvas.width/PTM - radius_meter) x_meter = canvas.width/PTM - radius_meter;

    bodyDef.set_position(new b2Vec2(x_meter, y_meter));
    var ball = {};
    ball.body = world.CreateBody(bodyDef);
    var shape = new b2CircleShape();
    shape.set_m_radius(radius_meter);
    ball.fixture = ball.body.CreateFixture(shape, 1);
    ball.fixture.SetRestitution(1);
    ball.fixture.SetFriction(0);
    ball.body.SetLinearVelocity(new b2Vec2(x_velocity, y_velocity));
    ball.body.SetAwake(1);
    ball.body.SetActive(1);
    return ball;
}

function endGame(message, title) {
	socket.disconnect();
    if (run) {
		pause();
	}
    var c = document.getElementById("canvas");
    c.style.display = 'none';
	var tutorial = document.getElementById("tutorial");
	tutorial.style.display = 'none';
	var statusList = document.getElementById("myStatus");
	statusList.style.display = 'none';
    var msg = document.getElementById("msg");
    msg.innerHTML = message;
    msg.style.display = 'inline';
    clearInterval(countDownTimer);
	clearInterval(epTimer);
	clearInterval(rotate_board);
	setTimeout(function () {document.title = title;}, 200);
}

var ibl = { //increaseBoardLength
    ep: 5,
    time: 10*1000,
	isActive: false
};
var rbh = { //recoverBlockHp
    ep: 10,
	amount: 5
};
var cbc = { //changeBallColor
    ep: 15,
    time: 15*1000,
	isActive: false
};
var laser = { //changeBallToLaser
	ep: 50,
	time: 5*1000, 
	isActive: false,
	isEnemyActive: false,
	extra_speed: 20
};
var ibd = { //increaseBallDamage
	ep: 10,
	time: 20*1000,
	isActive: false,
	ratio: 2.0
};

function increaseBoardLength(board) {
	var angle = board.body.GetAngle();
	var pos = board.body.GetPosition();
	board.width *= 2;
	board.body.GetWorld().DestroyBody(board.body);
	var bodyDef = new b2BodyDef();
	bodyDef.set_type(Module.b2_staticBody);
	bodyDef.set_position(new b2Vec2(pos.get_x(), pos.get_y()));
	board.body = world.CreateBody(bodyDef);
	var shape = new Box2D.b2PolygonShape();
	shape.SetAsBox(board.width / 2.0 / PTM, board.height / 2.0 / PTM);
	board.fixture = board.body.CreateFixture(shape, 1);
	board.body.SetTransform(pos, angle);
	window.setTimeout(function(board) {
		var angle = board.body.GetAngle();
		var pos = board.body.GetPosition();
		board.width /= 2.0;
		board.body.GetWorld().DestroyBody(board.body);
		var bodyDef = new b2BodyDef();
		bodyDef.set_type(Module.b2_staticBody);
		bodyDef.set_position(new b2Vec2(pos.get_x(), pos.get_y()));
		board.body = world.CreateBody(bodyDef);
		var shape = new Box2D.b2PolygonShape();
		shape.SetAsBox(board.width / 2.0 / PTM, board.height / 2.0 / PTM);
		board.fixture = board.body.CreateFixture(shape, 1);
		board.body.SetTransform(pos, angle);
	}.bind(this, board), ibl.time);
}

function recoverBlockHp(blockArray) {
	for (var key in blockArray) {
		var block = blockArray[key];
		block.hp += rbh.amount;
		//Allow limit break for 1 HP
		block.hp = Math.min(block.hp, block.type.hp + 1);
	}
}

function changeBallColor(changeBall, ToBall) {
	var originImage = changeBall.image.src;
	changeBall.image.src = ToBall.image.src;
	window.setTimeout(function(ball, img) {
		ball.image.src = img;
	}.bind(this, changeBall, originImage), cbc.time);
}

function changeBallToLaser(ball, mine) {
	ball.velocity += laser.extra_speed;
	updateVelocity(ball);
	if (mine) {
		laser.isActive = true;
		socket.emit("sync-laser", laser.isActive);
	}
	setTimeout(function () {
		ball.velocity -= laser.extra_speed;
		updateVelocity(ball);
		if (mine) {
			laser.isActive = false;
			socket.emit("sync-laser", laser.isActive);
		}
	}, laser.time);
}

function increaseBallDamage(ball) {
	ball.damage *= ibd.ratio;
	window.setTimeout(function () {
		ball.damage /= ibd.ratio;
	}, ibd.time);
}
	
