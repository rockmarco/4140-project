var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//var routes = require('./routes/index');
//var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var rooms = new Array(0);

//app.use('/', routes);
//app.use('/users', users);
app.get('/', function (req, res, next) {
    //res.render('index', { title: 'Home Page' });
    var num = -1;
    for (var key in rooms) {
		if (key > 999) {
			continue;
		}
        if (rooms[key].readyNum < 2 && !rooms[key].started) {
            num = parseInt(key);
            break;
        }
    }
    if (num == -1) {
        do {
            num = (Math.floor(Math.random() * 999) + 1);
        } while (rooms[num.toString()]);
        var obj = {
            readyNum: 0,
            started: false
        };
        rooms[num] = obj;
    }
    res.redirect('/' + num.toString());
});
app.get('/:id([0-9]+)', function(req, res, next) {
    //res.render('index', { title: 'Express' });
    res.sendFile(__dirname + '/views/testbed.html');
});

/*app.get('/game', function(req, res, next) {
	res.sendFile(__dirname + '/views/index.html');
});

app.get('/testbed', function(req, res, next) {
    res.sendFile(__dirname + '/views/testbed.html');
});*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var port = 44140;
var host = '0.0.0.0';

var server = app.listen(port, host, function() {
    console.log( 'Listening on http://%s:%s...', host, port );
} );

var io = require('socket.io')(server);
io.on('connection', function (socket) {
    console.log('User connected.');
    var id;
    var isReady = false;

    socket.on('disconnect', function(data) {
        if (isReady) {
            rooms[id].readyNum--;
            if (rooms[id].readyNum > 0) {
                io.sockets.in(id).emit('enemy-leave', null);
            } else {
                if (rooms[id]) {
                    delete rooms[id];
                }
            }
        }
    });
    socket.on('join', function(data) {
        socket.join(data);
        id = data;
        if (!rooms[id]) {
            var obj = {
                readyNum: 0,
                started: false
            };
            rooms[id] = obj;
        }
    });
    socket.on('updatePos', function(data) {
        socket.broadcast.to(id).emit('updatePos', data);
    });
    socket.on('updateAngle', function(data) {
        socket.broadcast.to(id).emit('updateAngle', data);
    });
    socket.on('ready', function(data) {
        if (rooms[id].readyNum == 2) {
            socket.emit('full', null);
        } else {
            isReady = true;
            rooms[id].readyNum++;
            if (rooms[id].readyNum == 2) {
                setTimeout(function() {
                    io.sockets.in(id).emit('start', null);
                }, 3*1000);
                io.sockets.in(id).emit('count-down', null);
                rooms[id].started = true;
            }
        }
    });
    socket.on('newBlock', function(data) {
        socket.broadcast.to(id).emit('newBlock', data);
    });
    socket.on('create-ball', function(data) {
        socket.broadcast.to(id).emit('create-ball', data);
    });
    socket.on('life-zero', function(data) {
        socket.broadcast.to(id).emit('life-zero', data);
    });
    socket.on('sync-ball', function(data) {
        socket.broadcast.to(id).emit('sync-ball', data);
    });
    socket.on('sync-laser', function(data) {
        socket.broadcast.to(id).emit('sync-laser', data);
    });
    socket.on('item-ibl', function(data) {
        socket.broadcast.to(id).emit('item-ibl', data);
    });
    socket.on('item-rbh', function(data) {
        socket.broadcast.to(id).emit('item-rbh', data);
    });
    socket.on('item-cbc', function(data) {
        socket.broadcast.to(id).emit('item-cbc', data);
    });
    socket.on('item-laser', function(data) {
        socket.broadcast.to(id).emit('item-laser', data);
    });
    socket.on('item-ibd', function(data) {
        socket.broadcast.to(id).emit('item-ibd', data);
    });
});

module.exports = app;
